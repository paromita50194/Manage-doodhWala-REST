package com.gwala.doodh.SampleGwala.entities;

import org.hibernate.annotations.BatchSize;
import org.springframework.format.annotation.DateTimeFormat;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Table(name="User_Master")
public class User {
    @Id
    @GeneratedValue
    @Column(name = "User_Id")
    private Integer id;
    @NotNull
    @Column(name = "User_Name")
    private String name;
    @NotNull
    @Column(name = "Address")
    private String address;
    @Pattern(regexp = "(^$|[0-9]{10})")
    @NotNull
    @Column(name = "Mobile_No")
    private String phoneNo;
    @Pattern(regexp = "(^$|[0-9]{10})")
    @Column(name = "Alternate_No")
    private String alternateNo;
    @ManyToOne(fetch=FetchType.EAGER)
    private Type   userType;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Column(name = "Created_Date")
    private Date created_date;
    @ManyToOne(fetch = FetchType.EAGER)
    private Status  activityStatus;
    @NotNull
    @Column(name = "Password")
    private String password;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAlternateNo() {
        return alternateNo;
    }

    public void setAlternateNo(String alternateNo) {
        this.alternateNo = alternateNo;
    }

    public Type getuserType() {
        return userType;
    }

    public void setuserType(Type type) {
        this.userType = type;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Status getActivityStatus() {
        return activityStatus;
    }

    public void setStatus(Status status) {
        this.activityStatus = status;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
