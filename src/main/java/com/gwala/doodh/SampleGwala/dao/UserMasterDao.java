package com.gwala.doodh.SampleGwala.dao;

import com.gwala.doodh.SampleGwala.entities.User;

public interface UserMasterDao {
    public User registerUser(User user);
}
