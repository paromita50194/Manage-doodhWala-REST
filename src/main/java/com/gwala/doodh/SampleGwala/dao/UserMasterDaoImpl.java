package com.gwala.doodh.SampleGwala.dao;

import com.gwala.doodh.SampleGwala.entities.Status;
import com.gwala.doodh.SampleGwala.entities.Type;
import com.gwala.doodh.SampleGwala.entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import java.util.List;

@Repository
public class UserMasterDaoImpl implements UserMasterDao {

    SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    /**
     * @param factory
     */
    @Autowired
    public UserMasterDaoImpl(EntityManagerFactory factory) {
        if(factory.unwrap(SessionFactory.class) == null){
            throw new NullPointerException("factory is not a hibernate factory");
        }
        this.sessionFactory = factory.unwrap(SessionFactory.class);
    }
    @Override
    public User registerUser(User user) {
     Session session= sessionFactory.openSession();
     session.getTransaction().begin();
     session.save(user);
     session.getTransaction().commit();
     user.getuserType().getId();
     String TypeByTypeID="select type from Type where id=:type_id";
     Query query=session.createQuery(TypeByTypeID);
        query.setParameter("type_id",user.getuserType().getId());
       List<String> ListofType= query.getResultList();
     //String type=   query
        Type type=new Type();
        type.setType(ListofType.get(0));
        user.setuserType(type);

        Query query1=session.createQuery("select status from Status where id=:status_id");
        query1.setParameter("status_id",user.getActivityStatus().getId());
        List<String> listOfStatus =query1.getResultList();
        Status status=new Status();
        status.setStatus(listOfStatus.get(0));
        user.setStatus(status);
        return user;
    }

}
