package com.gwala.doodh.SampleGwala.repositories;

import com.gwala.doodh.SampleGwala.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CrudRepository<User,Long> {
}
