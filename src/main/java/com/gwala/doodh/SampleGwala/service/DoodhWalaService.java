package com.gwala.doodh.SampleGwala.service;

import com.gwala.doodh.SampleGwala.dao.UserMasterDao;
import com.gwala.doodh.SampleGwala.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class DoodhWalaService {
    @Autowired
    UserMasterDao userMasterDao;

    @Transactional
    public User addUser(User user) {
        return userMasterDao.registerUser(user);
    }
}
