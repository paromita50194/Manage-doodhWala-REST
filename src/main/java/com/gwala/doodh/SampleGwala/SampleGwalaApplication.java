package com.gwala.doodh.SampleGwala;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleGwalaApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleGwalaApplication.class,args);
    }

}
