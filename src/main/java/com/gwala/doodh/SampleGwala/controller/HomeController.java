package com.gwala.doodh.SampleGwala.controller;


import com.gwala.doodh.SampleGwala.dao.UserMasterDaoImpl;
import com.gwala.doodh.SampleGwala.entities.User;
import com.gwala.doodh.SampleGwala.service.DoodhWalaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/doodhWala")
@Validated
public class HomeController {
    @Autowired
    DoodhWalaService doodhWalaService;

    @GetMapping(value = "/getDoodh")
    public String getReq() {
        return "doodh lee looo";
    }

    @PostMapping(value = "/addUser")
    public List<Object> addUser(@RequestBody User user) {
        List<Object> response = new ArrayList<>();
        doodhWalaService.addUser(user);
        response.add(user.getId());
        response.add(user.getActivityStatus().getStatus());
        response.add(user.getuserType().getType());
        return response;
    }

}
